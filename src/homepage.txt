                  THE PREEVENTUALIST'S LOSING AND FINDING REGISTRY
        (a free service benefiting the ENLIGHTENED who have been LIGHTENED)

                                    ---
                    updates are made daily, please check back!
                                    ---

                        this service is commissioned and
                    subsidized in part by The Ashley Raymond
                              Youth Study Clan

                                    ...
                    all seals and privileges have been filed
              under the notable authorship of Perry W. L. Von Frowling,
        Magistrate Polywaif of Dispossession.  Also, Seventh Straight Winner
                  of the esteemed Persistent Beggar's Community Cup.
                                    ...

ABOUT THE REGISTRY
==================
Hello, if you are new, please stay with us.  A brief explanation of our service will 
follow.  First, a bit of important news from our beloved magistrate.  (The kids call
him Uncle Von Guffuncle. Tehe!)

IMPORTANT NEWS
==============
/ 15 April 2005 /
hi, big news.  we were on channel 8 in wixl and ordish.  cory saw it.  i was on and 
jerry mathers was on.  if you didn't see it, e-mail cory.  he tells it the best.  all 
i can say is those aren't MY hand motions!! (joke for people who watch channel 8.)  
thanks harry and whole channel 8 news team!!
                                                  - perry

/ 07 April 2005 /
we're all sifting through the carpet here at hq, but if you could all keep an eye out 
for caitlin's clipboard, she's too quiet of a gal to post it and i know that it's 
REALLY important to her.  she had a few really expensive panoramic radiographs of her 
husband's underbite clipped to a few irreplacable photos of her husband in a robocop 
costume back when the underbite was more prominent.  she says (to me), "they'll know
what i mean when they see them."  i don't know what that means.  :(

i've checked: * the front desk * the hall * the waiting area * the bathroom * the candy 
closet * the big tv area * the lunch counter * the disciples room * gaff's old room
(the one with the painting of the cherry tree) * the server room * staircase.  i'll 
update this as i find more rooms.
                                                  - love, perry

/ 25 Feb 2005 /
server went down at 3 o'clock.  i'm mad as you guys.  gaff is downstairs and he'll 
be down there until he gets it fixed. :O -- UPDATE: it's fixed, back in bizz!!
                                                  - perry

/ 23 Feb 2005 /
i know there's a lot of noise today.  stanley bros circus lost twelve llamas and a
trailer and a bunch of Masterlocks and five tents.  they're still finding lost stuff.
pls keep your heads, i need everyone's help.  these entertainers have _nothing_.  i 
mean it.  i gave a guy a purple sticker today (it's just something i like to do as a
kind gesture) and he practically slept on it and farmed the ingredients for pizza sauce
on it.  they are on rock bottom.

so please donate.  i know we don't have paypal or anything.  so if you want to donate,
just post that you found something (a children's bike, a month of perishable canned 
goods) and that it has the circus people's names written on it or something.
                                                  - great, perry

/ 15 Nov 2004 /
preeventualist's day sale.  if you lose something today, you get to pick one free 
item (of $40 value or less) from the house of somebody who found something.  we're 
having so much fun with this!!  this is EXACTLY how i got my rowing machine last year
and i LOVE IT!!
                                                  - perry

USING THE L&F SERVER
====================
The L&F is a free service.  The acts of losing and finding are essential qualities in
building a preeventualist lifestyle.  We hope to accommodate your belief.

We do not use HTML, in order to simplify our work here.  Our guys are already working
fifteen hour days.  (Thanks, Terk!!  Thanks, Horace!!)

You may search our service for your lost items.  Or you may add your lost (or found)
item to our registry.  This is done by typing the proper address into your browser.

SEARCHING
=========
To search for lost items, use the following address:

  http://preeventualist.org/lost/search?q={search word}

You may replace {search word} with your search term.  For example, to search for "cup":

  http://preeventualist.org/lost/search?q=cup

You will be given a list of cups which have been lost or found.

If you want to search for only lost cups or only found cups, use the `searchlost' and
`searchfound' pages:

  http://preeventualist.org/lost/searchlost?q=cup