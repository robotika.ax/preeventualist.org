'use strict'
/**
 * THE PREEVENTUALIST'S LOSING AND FINDING REGISTRY
 *
 * @author Jan Lindblom <jan@robotika.ax>
 */

const AWS = require('aws-sdk')
const database_table = 'preeventualist'
const region = 'eu-north-1';
AWS.config.update({ region: region })
const dynamoDbClient = new AWS.DynamoDB()

exports.handler = function (event, context) {
  'use strict'
  const path = event.path
  const parameters = event.queryStringParameters ? event.queryStringParameters : {}

  if (path.startsWith('/lost/add') && path !== '/lost/add') {
    let is_lost = false
    if (path.endsWith('lost')) {
      // Add lost item
      is_lost = true
    } else if (path.endsWith('found')) {
      // Add found item
      is_lost = false
    }

    let name = parameters.name ? parameters.name : null
    let item = parameters.item ? parameters.item : null
    let at = null
    if (is_lost) {
      at = parameters.seen ? parameters.seen : null
    } else {
      at = parameters.at ? parameters.at : null
    }
    let desc = parameters.desc ? parameters.desc : null

    let reply = check_inputs(name, item, at, desc, is_lost)
    if (reply === '') {
      // Inputs ok, proceed.
      const added_date = new Date
      let params = createPutItemInput(generate_item_id(),
        Math.floor(added_date.getTime() / 1000),
        Math.floor(add_days(added_date, 28).getTime() / 1000), item, desc, at,
        is_lost ? 'lost' : 'found', name)
      return dynamoDbClient.putItem(params).promise().then(() => {
        context.succeed(generate_response(`Thanks for submitting your ${item}!`, 200))
        return
      }).catch((err) => {
        context.succeed(generate_response_with_content_type(JSON.stringify(err), 200, 'application/json'))
        return
      })
    } else {
      context.succeed(generate_response(reply, 200))
      return
    }

  } else if (path.startsWith('/lost/search' || path === '/lost/search')) {
    let query = parameters.q ? parameters.q : null

    if (query) {
      let lost_or_found = 'both'
      if (path.endsWith('lost')) {
        // Search lost
        lost_or_found = 'lost'
      } else if (path.endsWith('found')) {
        // Search found
        lost_or_found = 'found'
      }

      const params = lost_or_found === 'both' ? createQueryInput(query)
        : createLimitedQueryInput(query, lost_or_found)

      return dynamoDbClient.query(params).promise().then((result) => {

        if (result.Items) {
          let result_strings = []
          result.Items.forEach((item) => {
            result_strings.push(found_template(item.info.M.submitter_name.S,
              item.item_name.S, item.info.M.item_location.S,
              item.info.M.item_description.S))
          })
          if (result_strings.length > 0) {
            context.succeed(generate_response(result_strings.join('\n\n'), 200))
            return
          }
        }
        context.succeed(generate_response('No results found :(', 200))
        return
      }).catch(() => {
        context.succeed(generate_response('Oops, my bad, something went wrong :(', 500))
        return
      })
    } else {
      context.succeed(generate_response('You need to specify a search query with "?q="!', 400))
      return
    }
  } else {
    // Return the "homepage", with a generous caching setting whenever the
    // request was for something not adding or searching the registry.
    context.succeed({
      statusCode: 200,
      headers: {
        'Cache-Control': 'max-age=2628000, public', // Cache for a month
        'Content-Type': 'text/plain'
      },
      body: generate_homepage()
    })
    return
  }
}

/**
 * Generate a random id number.
 */
function generate_item_id() {
  const base = Math.floor((new Date).getTime() / 1000)
  return Math.floor(base + Math.random() * 10000)
}

/**
 * Generate a response with a given content type.
 *
 * @param {String} body the response body
 * @param {Number} status the response status code
 * @param {String} content_type the response content type
 */
function generate_response_with_content_type(body, status, content_type) {
  return {
    statusCode: status,
    headers: {
      'Cache-Control': 'max-age=3600, public',
      'Content-Type': content_type
    },
    body: body
  }
}

/**
 * Generate a general response.
 *
 * @param {String} body the response body
 * @param {Number} status the response status code
 */
function generate_response(body, status) {
  return generate_response_with_content_type(body, status, 'text/plain')
}

/**
 * Wrap object entry in a template.
 *
 * @param {String} name name of who found the item
 * @param {String} item name of the item
 * @param {String} at location of the item
 * @param {String} desc description of the item
 */
function found_template(name, item, at, desc) {
  return `${name}
--
${item}
--
${at}
--
${desc}`
}

/**
 * Checks inputs for adding items to registry.
 *
 * @param {String} name name of submitter
 * @param {String} item name of item
 * @param {String} at location of item
 * @param {String} desc description of item
 * @param {Boolean} is_lost if it's a lost item
 */
function check_inputs(name, item, at, desc, is_lost) {
  let message = 'You forgot to include the following fields: '
  let error_fields = []
  if (name === null) {
    error_fields.push('name')
  }
  if (item === null) {
    error_fields.push('item')
  }
  if (desc === null) {
    error_fields.push('desc')
  }
  if (at === null) {
    if (is_lost) {
      error_fields.push('seen')
    } else {
      error_fields.push('at')
    }
  }
  if (error_fields.length > 0) {
    return message + error_fields.join(', ')
  }
  return ''
}

/**
 * Adds the given number of days to a date and returns a new date in the
 * future.
 *
 * @param {Date} date a date
 * @param {Number} days a number of days
 */
function add_days(date, days) {
  const copy = new Date(Number(date))
  copy.setDate(date.getDate() + days)
  return copy
}

/**
 * Create a QueryInput object.
 *
 * @param {String} query search query
 */
function createQueryInput(query) {
  return {
    "TableName": database_table,
    "ScanIndexForward": false,
    "ConsistentRead": false,
    "KeyConditionExpression": "#d5f50 = :d5f50 And #d5f51 > :d5f51",
    "FilterExpression": "contains(#d5f52, :d5f52)",
    "ExpressionAttributeValues": {
      ":d5f50": {
        "S": "thing"
      },
      ":d5f51": {
        "N": "0"
      },
      ":d5f52": {
        "S": query
      }
    },
    "ExpressionAttributeNames": {
      "#d5f50": "object_type",
      "#d5f51": "item_id",
      "#d5f52": "item_name"
    }
  }
}

/**
 *Create a QueryInput object.

 * @param {String} query the search query
 * @param {String} lost_or_found either 'lost' or 'found'
 */
function createLimitedQueryInput(query, lost_or_found) {
  return {
    "TableName": database_table,
    "ScanIndexForward": false,
    "ConsistentRead": false,
    "KeyConditionExpression": "#39e10 = :39e10 And #39e11 > :39e11",
    "FilterExpression": "contains(#39e12, :39e12) And #39e13 = :39e13",
    "ExpressionAttributeValues": {
      ":39e10": {
        "S": "thing"
      },
      ":39e11": {
        "N": "0"
      },
      ":39e12": {
        "S": query
      },
      ":39e13": {
        "S": lost_or_found
      }
    },
    "ExpressionAttributeNames": {
      "#39e10": "object_type",
      "#39e11": "item_id",
      "#39e12": "item_name",
      "#39e13": "item_type"
    }
  }
}

/**
 * Creates a PutItem input object.
 *
 * @param {Number} item_id an item ID.
 * @param {Number} added_at an epoch date number
 * @param {Number} expires an epoch expiry date number.
 * @param {String} name name of submitter.
 * @param {String} description description of item.
 * @param {String} location location of item.
 * @param {String} type type of item (lost/found).
 * @param {String} submitter name of submitter.
 */
function createPutItemInput(item_id, added_at, expires, name, description, location, type, submitter) {
  return {
    "TableName": database_table,
    "Item": {
      "object_type": {
        "S": "thing"
      },
      "item_name": {
        "S": name
      },
      "item_type": {
        "S": type
      },
      "item_id": {
        "N": item_id.toString()
      },
      "added_at": {
        "N": added_at.toString()
      },
      "expires_at": {
        "N": expires.toString()
      },
      "info": {
        "M": {
          "submitter_name": {
            "S": submitter
          },
          "item_description": {
            "S": description
          },
          "item_location": {
            "S": location
          }
        }
      }
    }
  }
}

/**
 * Generate the "homepage"
 */
function generate_homepage() {
  let message = `                   THE PREEVENTUALIST'S LOSING AND FINDING REGISTRY
          (a free service benefiting the ENLIGHTENED who have been LIGHTENED)

                                      ---
                      updates are made daily, please check back!
                                      ---

                          this service is commissioned and
                      subsidized in part by The Ashley Raymond
                                Youth Study Clan

                                      ...
                      all seals and privileges have been filed
                under the notable authorship of Perry W. L. Von Frowling,
          Magistrate Polywaif of Dispossession.  Also, Seventh Straight Winner
                    of the esteemed Persistent Beggar's Community Cup.
                                      ...

 ABOUT THE REGISTRY
 ==================
 Hello, if you are new, please stay with us.  A brief explanation of our service will
 follow.  First, a bit of important news from our beloved magistrate.  (The kids call
 him Uncle Von Guffuncle. Tehe!)

 IMPORTANT NEWS
 ==============
 / 15 April 2005 /
 hi, big news.  we were on channel 8 in wixl and ordish.  cory saw it.  i was on and
 jerry mathers was on.  if you didn't see it, e-mail cory.  he tells it the best.  all
 i can say is those aren't MY hand motions!! (joke for people who watch channel 8.)
 thanks harry and whole channel 8 news team!!
                                             - perry

 / 07 April 2005 /
 we're all sifting through the carpet here at hq, but if you could all keep an eye out
 for caitlin's clipboard, she's too quiet of a gal to post it and i know that it's
 REALLY important to her.  she had a few really expensive panoramic radiographs of her
 husband's underbite clipped to a few irreplacable photos of her husband in a robocop
 costume back when the underbite was more prominent.  she says (to me), "they'll know
 what i mean when they see them."  i don't know what that means.  :(

 i've checked: * the front desk * the hall * the waiting area * the bathroom * the candy
 closet * the big tv area * the lunch counter * the disciples room * gaff's old room
 (the one with the painting of the cherry tree) * the server room * staircase.  i'll
 update this as i find more rooms.
                                             - love, perry

 / 25 Feb 2005 /
 server went down at 3 o'clock.  i'm mad as you guys.  gaff is downstairs and he'll
 be down there until he gets it fixed. :O -- UPDATE: it's fixed, back in bizz!!
                                             - perry

 / 23 Feb 2005 /
 i know there's a lot of noise today.  stanley bros circus lost twelve llamas and a
 trailer and a bunch of Masterlocks and five tents.  they're still finding lost stuff.
 pls keep your heads, i need everyone's help.  these entertainers have _nothing_.  i
 mean it.  i gave a guy a purple sticker today (it's just something i like to do as a
 kind gesture) and he practically slept on it and farmed the ingredients for pizza sauce
 on it.  they are on rock bottom.

 so please donate.  i know we don't have paypal or anything.  so if you want to donate,
 just post that you found something (a children's bike, a month of perishable canned
 goods) and that it has the circus people's names written on it or something.
                                             - great, perry

 / 15 Nov 2004 /
 preeventualist's day sale.  if you lose something today, you get to pick one free
 item (of $40 value or less) from the house of somebody who found something.  we're
 having so much fun with this!!  this is EXACTLY how i got my rowing machine last year
 and i LOVE IT!!
                                             - perry

 USING THE L&F SERVER
 ====================
 The L&F is a free service.  The acts of losing and finding are essential qualities in
 building a preeventualist lifestyle.  We hope to accommodate your belief.

 We do not use HTML, in order to simplify our work here.  Our guys are already working
 fifteen hour days.  (Thanks, Terk!!  Thanks, Horace!!)

 You may search our service for your lost items.  Or you may add your lost (or found)
 item to our registry.  This is done by typing the proper address into your browser.

 SEARCHING
 =========
 To search for lost items, use the following address:

   http://preeventualist.org/lost/search?q={search word}

 You may replace {search word} with your search term.  For example, to search for "cup":

   http://preeventualist.org/lost/search?q=cup

 You will be given a list of cups which have been lost or found.

 If you want to search for only lost cups or only found cups, use the \`searchlost' and
 \`searchfound' pages:

   http://preeventualist.org/lost/searchlost?q=cup`;
  return message;
}
